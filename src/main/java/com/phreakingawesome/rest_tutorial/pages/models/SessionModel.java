package com.phreakingawesome.rest_tutorial.pages.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.phreakingawesome.rest_tutorial.security.Session;

public class SessionModel {
  private String sessionID = "";
  private String uname = "";
  
  public SessionModel() {}
  
  public SessionModel(Session session) {
    sessionID = session.getSessionID().toString();
    uname = session.getUsername();
  }
  
  @JsonProperty("session")
  public String getSessionID() {
    return sessionID;
  }
  
  @JsonProperty("session")
  public void setSessionID(String sessionID) {
    this.sessionID = sessionID;
  }
  
  @JsonProperty("username")
  public String getUname() {
    return uname;
  }
  
  @JsonProperty("username")
  public void setUname(String uname) {
    this.uname = uname;
  }
}
