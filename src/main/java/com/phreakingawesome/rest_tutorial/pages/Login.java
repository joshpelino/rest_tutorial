package com.phreakingawesome.rest_tutorial.pages;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.phreakingawesome.rest_tutorial.Main;
import com.phreakingawesome.rest_tutorial.pages.models.SessionModel;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.*;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("login")
public class Login {
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response login(String request) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      String passw = mapper.readTree(request).get("password").asText();
      String uname = mapper.readTree(request).get("username").asText();
      SessionModel sessionId = new SessionModel(Main.MAIN.checkPassword(uname, passw, "127.0.0.1"));
      //GenericEntity entity = new GenericEntity<SessionModel>(sessionId){};
      return Response.status(200).entity(sessionId).build();
    } catch (IOException | NullPointerException e) {
      e.printStackTrace();
    }
  
  return Response.status(400).build();
  
  }
}
