package com.phreakingawesome.rest_tutorial.pages;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;

@Path("poke")
public class TestApi {
  
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public Response hello() {
    return Response.status(200).entity("Hello, Dave.").build();
  }
  
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public Response multiply(String request) {
    ObjectMapper mapper = new ObjectMapper();
        int num1 = 0, num2 = 0;
    
    try {
      num1 = mapper.readTree(request).get("num1").asInt();
      num2 = mapper.readTree(request).get("num2").asInt();
      return Response.status(200).entity("{\"product\": " + (num1 * num2) + "}").build();
    } catch (IOException e) {
      e.printStackTrace();
    }
  
    return Response.status(400).build();
  }
}
