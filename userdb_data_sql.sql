USE users;

drop table if exists users;
# drop table if exists privilege_levels;

# create table privilege_levels
# (
#   plevel  int         not null primary key,
#   pname   varchar(12) not null
# );

create table users
(
  id        int           auto_increment primary key,
  firstName varchar(50)   not null,
  lastName  varchar(50)   not null,
  email     varchar(50)   not null,
  plevel    int           not null,
  salt      varchar(8)    not null,
  password  varchar(128)  not null
);

INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1000, 'Clyde', 'Feit', 'clyde.feit.530@sbcglobal.net', 3, 'cbc4abb8', 'd30852e45b777a0384f3146dbe2bc7f7df5bd3e9afa59ac574dd2372a16665033059e87cf04aaa9840413080df180c1ac76dcc6f5104906488ec2c3d720e7f33');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1001, 'Rashida', 'Dagley', 'rashida.dagley.939@sbcglobal.net', 3, 'd2232ff4', 'c192b07f32adf8539bd40c70118e8824af1afa156d3a1057683aad9dccb3a4dc92dee353db6f4a5d83db50b3d82b74d37b31f0bd51518fbff01bcf2300187262');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1002, 'Francisco', 'Conda', 'francisco.conda.135@comcast.net', 3, 'e92c0242', '36df1b624511abed0122e75dde12bfa787721612a25e0e009a8a2606a8912458ea9364490a1eac7431fa154ba288e3149fc5a9f2f9c8da2369ed932ab7e0f134');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1003, 'Columbus', 'Courtenay', 'columbus.courtenay.483@att.net', 3, 'd06e7fc9', 'fb49f6c93165e5af4ae4e3f2bfabecf3ed2272f4f521e6b2fcbc5da8f976c87d96e8476f8661f073944bfa53f591ef8550bb18ac2b360cf2c63e22e57504facd');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1005, 'Minh', 'Khoun', 'minh.khoun.695@earthlink.net', 3, 'e6109d6f', 'acd33585d6f0ba34f56539faf57599c6037b48ad547ddf7ea6a569e0266c8152368423f34a437a76d7d8fb645155d1d2ee441a35b9ab72a5be4c979c91b3f831');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1006, 'Oliver', 'Branaugh', 'oliver.branaugh.705@hotmail.co.uk', 3, 'fa0dd996', '27ce38ee3e66668302bb8aecc88e269577104860b2da4bf4b6358f2b514735f114a50ae7eca021aa28fb6799cfead657f64fbf9cebf7902a820922d2c0b3d3b8');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1007, 'Rogelio', 'Shoe', 'rogelio.shoe.280@juno.com', 3, 'cdaecc9a', 'c0d7c093e8085c97c1786db03767c357ae128c78bca3aef68713422a968094ec31fc1bf4d7aa66e570f15c74e29a972021bf8ac4bab67269869569189f74e49b');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1008, 'Bobby', 'Boxx', 'bobby.boxx.865@live.com', 3, 'd77ae05c', '98e6a7405a7949f1bce1f1986fe84f2c6c5f28944bceb83a0f97bc01c7585f63f87d221e62c1aa7067918beba7640e395f481f823567923291b22dcc49fe4711');
INSERT INTO users.users (id, firstName, lastName, email, plevel, salt, password) VALUES (1009, 'Carroll', 'Flockerzi', 'carroll.flockerzi.623@hotmail.com', 3, 'ddb1c3ce', 'cb24b114fc236454c21ccfcc039c569f7de8d9a64557bf08165d7b43d72dcd8eb8d5f620933c9dbce17e939694af4ab2295564f14371f6cbb4716ba58d41548b');